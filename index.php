<?php
include("services/auth.php");
?>
<!DOCTYPE html>
<html lang="en" ng-app="NibVoating">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>NIC SHMF Application</title>
    <link rel="shortcut icon" href="img/NIBLogo.png" type="image/x-icon">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/owl.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="dist/css/progressbar.css">
    <link rel="stylesheet" href="css/checkbox.css">
    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.css">
    <link rel="stylesheet" href="dist/css/dataTable.css">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/select2.min.css">
</head>
<body>
        <input id="username" type="hidden" name="input" value="<?php echo $_SESSION['username'] ?>" ng-model="username"/>
        <?php
        if($_SESSION['role'] != "Guest"){
        ?>
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a style="color:rgb(236, 6, 116)" class="navbar-brand" href="#/"><img style="float: left;" class="img img-responsive" width="30" height="30" src="img/NIBLogo.png"/><label style="float: left; margin-top:7px; margin-left:5px;">NIC SHMF Application</label></a>
                <!-- SHMF Shareholders Meeting Fasilitator-->
            </div>

            <ul class="nav navbar-top-links navbar-right">
                        <li class="divider">
                            <i class="fa fa-user fa-fw"></i> Hello  <?php echo $_SESSION['username'] ?>,
                        </li>
                        <li class="divider">
                            <a href="#/changePassword"><i class="fa fa-cog fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider">
                        <a href="services/logout.php"><i class="fa fa-sign-out-alt fa-fw"></i> Logout</a>
                        </li>
            </ul>
            <!-- /.navbar-header -->
           <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <a href="#/" style="font-size: 18px; text-decoration: none; color: rgb(2, 2, 68)"><i class="menu-icon fa fa-tachometer-alt"></i><span style="padding-left: 20px "> Dashboard</span></a>
                            </div>
                            <!-- /input-group -->
                        </li>


                        <?php
                        if($_SESSION['role'] == "Administrator"){
                        ?>
                         <li class="menu-item">
                            <a href="#/addshareholder"><i class="menu-icon fa fa-users"></i> <span style="padding-left: 17px">Add Shareholder</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                         <li class="menu-item">
                            <a href="#/addcandidate"><i class="menu-icon fa fa-user-plus"></i> <span style="padding-left: 17px">Add Candidate</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        </li>
                          <li class="menu-item">
                            <a href="#/agendaControl"><i class="menu-icon fa fa-handshake"></i> <span style="padding-left: 17px">Add Agenda</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        </li>
                          <li class="menu-item">
                            <a href="#/noticeControl"><i class="menu-icon fa fa-clipboard"></i> <span style="padding-left: 17px">Add Notice</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php
                        }
                        ?>


                        <?php
                        if($_SESSION['role'] == "Administrator" || $_SESSION['role'] == "Registrar"){
                        ?>
                        <li class="menu-item">
                            <a href="#/attendance"><i class="menu-icon fa fa-clipboard-list"></i> <span style="padding-left: 17px">Attendance</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php
                        }
                        ?>         
                        <?php
                        if($_SESSION['role'] == "Administrator" || $_SESSION['role'] == "Voter"){
                        ?>
                          <li class="menu-item">
                            <a href="#/voteregistration"><i class="menu-icon fa fa-allergies"></i> <span style="padding-left: 17px">Register Votes</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                         </li>
                        <?php
                        }
                        ?> 
                      


                        <?php
                        if($_SESSION['role'] == "Administrator" || $_SESSION['role'] == "RegAuditor"){
                        ?>
                        <li class="menu-item">
                            <a href="#/attendanceapproval"><i class="menu-icon fa fa-calendar-check"></i> <span style="padding-left: 17px">Approve Attendance</span></a>
                            <!-- /.nav-second-level -->
                         </li>
                        <?php
                        }
                        ?>
                        <?php
                        if($_SESSION['role'] == "Administrator" || $_SESSION['role'] == "VoteAuditor"){
                        ?>
                        <li class="menu-item">
                            <a href="#/voteapproval"><i class="menu-icon fa fa-check-circle"></i> <span style="padding-left: 17px">Approve Votes</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="menu-item">
                            <a href="#/agendaVote"><i class="menu-icon fa fa-handshake"></i> <span style="padding-left: 17px">Vote For Agenda</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php
                        }
                        ?>

                        <?php
                        if($_SESSION['role'] == "Administrator"){
                        ?>
                          <li class="menu-item">
                            <a href="#/usercontrol"><i class="menu-icon fa fa-user"></i> <span style="padding-left: 17px">Manage Users</span></a>
                            <!-- /.nav-second-level -->
                        </li>

                        </li>
                          <li class="menu-item">
                            <a href="#/votereverse"><i class="menu-icon fa fa-reply"></i> <span style="padding-left: 17px">Board Votes Reversal</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="menu-item">
                            <a href="#/agendavotereverse"><i class="menu-icon fa fa-reply"></i> <span style="padding-left: 17px">Agenda Votes Reversal</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                         <li class="menu-item">
                            <a href="#/proxy"><i class="menu-icon fa fa-user-tag"></i> <span style="padding-left: 17px">Manage Proxy</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        </li>
                        <li>
                            <a href="#"><i class="menu-icon fa fa-fw fa-file-alt"></i><span style="padding-left: 18px">Reports</span><i style="float:right" class="fa fa-arrow-down"></i></a>
                            <ul class="nav nav-second-level">
                                 <li>
                                    <a href="#/ShareholdersAttendance"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 10px">Attendance Report</span></a>
                                </li>
                                <li>
                                    <a href="#/BoardVoteReport"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 8px"> Board Vote Report</span></a>
                                </li>
                                <li>
                                    <a href="#/AgendaVoteReport"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 10px">Vote on Agenda Report </span></a>
                                </li>
                                <li>
                                    <a href="#/ReversedVoteReport"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 8px"> Reversed Votes Report</span></a>
                                </li>
                                 <li>
                                    <a href="#/ProxyReport"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 8px"> Proxy Attn. Report </span></a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php
                        }
                        ?>

                     </ul>
                <!-- /.sidebar-collapse -->
                 </div>
            </div>
           <!-- /.navbar-static-side -->
        </nav>
        <?php
            }
        ?>
       <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                       <?php
                            if($_SESSION['role'] == "Guest"){
                        ?>
                            <ul style="margin-top: 25px; " class="nav navbar-top-links navbar-right">
                            <li class="divider">
                                <a href="services/logout.php"><i class="fa fa-sign-out-alt fa-fw"></i> Logout</a>
                            </li>
                            </ul>
                        <?php
                        }
                        ?>
                    <h2 style="margin-left: 10px" class="page-header"><i class="menu-icon fa fa-tachometer-alt"></i> NIC Dashboard</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div style="min-height: 100%; background-color: white; padding: 10px" class="row">
                <ng-view>
        
                </ng-view>
                <div style="vertical-align: bottom; text-align: center; bottom: 0px;">
                <hr>
                <label><span style="font-weight:300"> © </span> Designed and Developed By Hawariyaw Pawulos</label>
                </div>
            </div>
        </div>

    <script src="scripts/jquery.min.js"></script>
    <script src="scripts/wow.js"></script>

    <!--contact js-->
    <script src="scripts/custom.js"></script>
    <script src="scripts/jquery.easing.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <script src="scripts/fontawesome.js"></script>
    <script src="scripts/fontawesome-all.js"></script>
     <!-- DataTables JavaScript -->
     <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
     <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
     <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
     <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
            $('#dataTables-example').DataTable({
            responsive: true
        });
        });
    </script> 
    <script src="scripts/select2.min.js"></script> 
    <script src="scripts/angular.js"></script>
    <script src="scripts/angular.min.js"></script>
    <script src="scripts/angular.js"></script>
    <script src="scripts/angular-route.js"></script>
    <script src="scripts/progressbar.js"></script>
    <script src="scripts/sweetAlert.js"></script>
    <script src="scripts/dirPagination.js"></script>
    <script src="controllers/controller.js"></script>  
</body>
</html>
