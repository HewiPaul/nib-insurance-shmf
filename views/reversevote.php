<?php
include("../services/auth.php");
?>

 <div class="panel panel-default">
                <div class="panel-heading">
                     Reverse Vote
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                   <div style="padding-right:15px" class="row">
                                <form class="form-inline">
                                    <div style="float:right; padding-top: 15px; padding-bottom: 15px" class="input-group">
                                        <input class="form-control" type="text" ng-model="search2" placeholder="Search"
                                            type="search" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-search"></span>
                                        </span>
                                    </div>
                                </form>
                   </div>
                <div style="padding: 10px;" class="row">
                <label id="username" style="display: none; visibility: hidden;"><?php echo $_SESSION['username']; ?></label>
                <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                        <thead>
                            <tr>
                             <td>Id</td>
                             <td>Voter Id</td>
                             <td>Voter Name</td>
                             <td>Voted Candidates Id</td>
                             <td>Voted Candidates Name</td>
                             <td>Voter Subscribed Share</td>
                             <td>Data Encoder</td>
                             <td>Action</td>
                       </tr>
                     </thead>
                     <tbody>
                         <tr ng-repeat="row in votes|filter:search2" ng-model="search2">                                        
                             <td>{{row.Id}}</td>
                             <td>{{row.VoterId}}</td>
                             <td>{{row.VoterName}}</td>
                             <td>{{row.VotedCandidatesId}}</td>
                             <td>{{row.VotedCandidatesName}}</td>
                             <td>{{row.SubscribedShare}}</td>
                             <td>{{row.DataEncoder}}</td>
                             <td>
                              <a class="btn btn-danger" type="button" name="delete" ng-click="reverse(row.Id)"><i class="fa fa-reply"></i>
                              </a>
                             </td>
                         </tr>
                     </tbody>
                </table>
            </div>
          </div> 
  </div>