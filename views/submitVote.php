<?php
include("../services/auth.php");
?>
<link rel="stylesheet" href="dist/css/select2.css">

<div class="page-header">
    <h1>Vote Registration</h1>
</div> 

<form role="form" ng-submit="GetSelectedvalues()">
<div  style="padding:8px;" class="row">
<div class="form-group col-xs-6">
		<label for="shareholdername">Voter Name</label>
		<select id="voaterId" class="js-example-basic-single form-control" name="state" class="form-control" ng-model="voaterId" data-ng-change="setValues(voaterId)" data-ng-options="shareholder as shareholder.Id for shareholder in shareholders" 
		ng-value="{{shareholder.Id}}" data-ng-required="true">
        <option value="" selected disabled hidden>Select Voter</option>
        </select>
 <br><br>
 <label for="shareholdername">Shareholder Name</label>: <label style="color:green" id="shareholderName">{{voterName}}</label>
 <br><br>
 <label for="shareholdername">Subscribed Share</label>: <label style="color:green" id="NumberOfShare">{{noofshareholder}}</label>
 <br><br>
 <label for="amount">Amount</label>: <label style="color:green" id="amount">{{amount}}</label>
</div>
<div class="form-group col-xs-6">
    <?php
      if($_SESSION['role'] == "Administrator"){
    ?>
	<label for="shareholdername">Max Vote</label>
	<select id="maxVote" class="js-example-basic-single form-control" name="state" class="form-control" ng-model="maxVote" data-ng-change="setMaxVote(maxVote)"  data-ng-required="true">
	<option value="" selected disabled hidden>Select Max Vote</option>
	<option value="3">Three (3)</option>
	<option value="6">Six (6)</option>
	<option value="9">Nine (9)</option>
	</select>
	<br>
	<div style="padding-top:15px" class="form-group">
	<label class="label-big-check">	
	<input id="VoterType" type="checkbox" name="VoterType" ng-model="voterType.checked" ng-click="setVoteType(voterType)"/>
	<label for="VoterType" class="check-title">Non-Influential Shareholders Only</label>
	</label>
	</div>
	<?php
    }
	?>
	<?php
      if($_SESSION['role'] != "Administrator"){
    ?>
	<br>
	<label style="margin-top:10px; color:green; font-weight:bold">Maximum Vote is Set To: <span style="color:red">{{maxVote}}</span></label>
	<?php
    }
	?>
</div>
</div>
<div style="padding: 5px;" class="row">
<div ng-if="$index+1 <= HalfCandidates" class="form-group col-md-6" ng-repeat="candidate in candidates">
<label class="label-big-check">	
	<input type="checkbox" name="selectedCandidates[]" value="{{candidate.Id}}" id="{{candidate.Id}}" ng-model="candidate.checked" ng-click="check(candidate, $index)" ng-disabled="!candidate.checked && checkedNumber === limitNumber" data-ng-required="checkedNumber <= 0">
	<label for="{{candidate.Id}}" class="check-title"><i class="fa fa-user"></i>  {{candidate.Name}}</label>
</label>
</div>
<div ng-if="$index+1 > HalfCandidates" class="form-group col-md-6" ng-repeat="candidate in candidates">
<label class="label-big-check">	
	<input type="checkbox" name="selectedCandidates[]" value="{{candidate.Id}}" id="{{candidate.Id}}" ng-model="candidate.checked" ng-click="check(candidate, $index)" ng-disabled="!candidate.checked && checkedNumber === limitNumber" data-ng-required="checkedNumber <= 0">
	<label for="{{candidate.Id}}" class="check-title"><i class="fa fa-user"></i>  {{candidate.Name}}</label>
</label>
</div>
</div>

<div style="padding:6px;" class="row">
	<div class="col-xs-6">
		<input class="btn btn-primary pull-left" type="Submit" ng-click="checkMinimum()" value="Submit Vote &raquo;" />
	</div>
	<div class="col-xs-6">
		<a href="#/" class="btn btn-default pull-right" role="button"><span class="glyphicon glyphicon-arrow-left"></span> Back to Home</a>
	</div>
</div>
</form>

<script src="scripts/select2.min.js"></script>
<script>
      // In your Javascript (external .js resource or <script> tag)
      $(document).ready(function () {
            $('.js-example-basic-single').select2();
            $('#files').change(handleFile);
      });
</script>