<?php
include("../services/auth.php");
?>
<link rel="stylesheet" href="dist/css/select2.css">
<div class="page-header">
    <h1>Vote Approval</h1>
</div> 

<div class="panel panel-default">
                <div class="panel-heading">
                   Votes To Be Approved
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                <label id="username" style="display: none; visibility: hidden;"><?php echo $_SESSION['username']; ?></label>
               
                <div style="float:right; padding-top: 15px; padding-bottom: 15px; padding-right: 10px;" class="input-group">
                        <span class="input-group-addon">
                                    <span>Maker</span>
                        </span>
                        <select style="width:250px" id="voter" class="form-control"
                            ng-model="maker" disabled>
                                <option value="{{maker}}" selected>{{maker}}</option>
                        </select>
                </div>

                   <div style="padding-right:15px" class="row">
                                <form class="form-inline">
                                    <div style="float:right; padding-top: 15px; padding-bottom: 15px" class="input-group">
                                        <input class="form-control" type="text" ng-model="search" placeholder="Search"
                                            type="search" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-search"></span>
                                        </span>
                                    </div>
                            </form>
                   </div>

                <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                        <thead>
                            <tr>
                             <td>Id</td>
                             <td>Voter Id</td>
                             <td>Voter Name</td>
                             <td>Voted Candidates</td>
                             <td>Voter Subscribed Share</td>
                             <td>Requested By</td>
                             <td>Actions</td>
                       </tr>
                     </thead>
                     <tbody>
                         <tr dir-paginate="row in votes | filter: maker : FilterMaker|filter: search: myFilter |itemsPerPage:15" pagination-id="row">
                             <td>{{row.Id}}</td>
                             <td>{{row.VoterId}}</td>
                             <td>{{row.VoterName}}</td>
                             <td>
									<ul style="list-style: none;" ng-repeat="shareholder in  (row.VotedCandidates | customSplitString)">
  									 <li ng-if="!$last"><input type="checkbox" name="" checked disabled /> {{ shareholder }}</li>
   									</ul>
                             </td>
                             <td>{{row.NumberOfShare}}</td>
                             <td>{{row.MakerName}}</td>
                             <td style="white-space: nowrap">
                             	<a class="btn btn-success" type="button" name="approve" ng-click="approvevote(row.Id)"><i class="fa fa-check-square"></i></a>
                                <a class="btn btn-danger" type="button" name="delete" ng-click="deletevote(row.Id)"><i class="fa fa-trash-alt"></i></a>
                             </td>
                         </tr>
                     </tbody>
                </table>
                <dir-pagination-controls pagination-id="row" max-size="15" direction-links="true" boundary-links="true">
                </dir-pagination-controls>
          </div> 
  </div>

<script src="scripts/select2.min.js"></script>
  <script>
   $(document).ready(function () {
        $('.js-example-basic-single').select2();
    });
</script>