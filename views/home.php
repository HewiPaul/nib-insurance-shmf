<?php
include("../services/auth.php");
?>
<style>
#container {
  margin: 20px;
  width: 200px;
  height: 200px;
  position: relative;
}
#container2 {
  margin: 20px;
  width: 200px;
  height: 200px;
  position: relative;
}
#container3 {
  margin: 5px;
  width: 50px;
  height: 50px;
  position: relative;
}
</style>
<link href="https://fonts.googleapis.com/css?family=Raleway:400,300,600,800,900" rel="stylesheet" type="text/css">




<div class="row">
<div class="panel panel-default">
                  <div class="panel-heading">
                        Display
                  </div>
                  <!-- /.panel-heading -->
                  <div class="panel-body">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                             <li class="active">
                                    <a href=".maindisplay" data-toggle="tab">Agenda Display</a>
                              </li>
                              <li>
                                    <a href=".Quorum" data-toggle="tab">Shareholders Quorum</a>
                              </li>
                              <li>
                                    <a href=".voteResult" data-toggle="tab">Board Vote Status</a>
                              </li>
                              <li>
                                    <a href=".voteonAgendaResult" data-toggle="tab">Vote On Agenda Status</a>
                              </li>
                              <li>
                                    <a href=".NoticeBoard" data-toggle="tab">Notice Board</a>
                              </li>
                        </ul>
<div style="padding:10px" class="tab-content">
<div class="tab-pane fade in active maindisplay">
<h2> Todays Agenda (የ16ኛው መደበኛ እና የ12ኛው ድንገተኛ ጠቅላላ ጉባኤ አጀንዳዎች)</h2>
    <p class="lead">
        This are the agendas that will gone covered on todays meeting!
    </p>
    <div class="row" ng-repeat="row in agendas">
        <div ng-if="row.LetsVoat == '0'" class="alert alert-info">
            <h4 style="font-weight: bolder;">{{row.Agenda}}</h4>
            <p>{{row.AgendaDesc}}</p><br>
            <?php
                if($_SESSION['role'] == "Administrator"){
            ?>
            <input style="background-color: gray; border-color: gray" ng-if="row.LetsVoat != '0'" class="btn btn-warning" type="button" name="button" ng-click="makeDefaultToVote(row.Id)" value="Vote For This" disabled />
            <input ng-if="row.LetsVoat == '0'" class="btn btn-warning" type="button" name="button" ng-click="makeDefaultToVote(row.Id)" value="Vote For This"/>
            <?php
              }
            ?>
        </div>

        <div ng-if="row.LetsVoat == '1'" class="alert alert-warning">
        <h4 style="font-weight: bolder;">{{row.Agenda}}</h4>
        <p>{{row.AgendaDesc}}</p><br>
        <?php
            if($_SESSION['role'] == "Administrator"){
        ?>
        <input style="background-color: gray; border-color: gray" ng-if="row.LetsVoat != '0'" class="btn btn-warning" type="button" name="button" ng-click="makeDefaultToVote(row.Id)" value="Vote For This" disabled />
        <input ng-if="row.LetsVoat == '0'" class="btn btn-warning" type="button" name="button" ng-click="makeDefaultToVote(row.Id)" value="Vote For This"/>
        <?php
          }
        ?>
        </div>
  </div>
</div>

<div class="tab-pane fade in Quorum">
<div class="row">
 <div class="col-md-6">
        <div class="panel panel-info">
            <div style="min-height: 300px" class="panel-heading">
                <div style="margin: auto" class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="row">
                        <div><h2>Attended Shareholders</h2></div>
                        <div style="padding-left:10ppx; padding-right:30px; float: right" class="col-md-9">
                             <div id="container"></div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="panel-footer">
                    <span class="pull-left  comment-dots">Number Of Attended Shareholders</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
        </div>
    </div>

     <div class="col-md-6">
        <div class="panel panel-warning">
            <div style="min-height: 300px" class="panel-heading">
                <div style="margin: auto" class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="row">
                       <div><h2>Quorum Percentage</h2></div>
                        <div style="padding-left:10ppx; padding-right:30px; float: right" class="col-md-9">
                             <div id="container2"></div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="panel-footer">
                    <span class="pull-left">Quorum Percentage</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
        </div>
    </div>
  </div>
    </div>

<div class="tab-pane fade in voteResult">
<div class="row">
<label class="label-big-check" ng-repeat="candidate in candidates">
  <label>{{$index + 1}}</label>
  <label for="{{candidate.Id}}" class="check-title"><i style="margin-right:10px" class="fa fa-user"></i>  {{candidate.Name}}</label>
  <label style="float:right"> {{candidate.votes}} Votes</label>
  <div class="progress">
      <div id="progressbar"ng-if="$index+1 <= candidateNumber/2" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{((candidate.votes*1) * 100/votes)}}%;padding-left: 10px; color: rgb(51, 51, 0); background: rgb(204, 255, 204)">{{((candidate.votes*1) * 100/votes) | number : 2}}%
      </div>
       <div ng-if="$index+1 > candidateNumber/2" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{((candidate.votes*1) * 100/votes)}}%;padding-left: 10px; color: rgb(0, 51, 102); background: rgb(255, 204, 224)">{{((candidate.votes*1) * 100/votes) | number : 2}}%
      </div>
  </div>
</label>
</div>
</div>


<div class="tab-pane fade in voteonAgendaResult">
 <div class="row">
 <div class="accordion{{row.Id}}" class="panel-group wow fadeInUpBig" data-wow-delay="0.4s" ng-repeat="row in agendasVote">
                <div class="panel panel-default">
                        <a style="text-decoration:none; width: 100%; padding: 20px" class="btn btn-link panel-heading" data-toggle="collapse" data-parent=".accordion{{row.Id}}" href=".collapse{{row.Id}}">
                                <div class="panel-heading">
                                    <div style="float: left; width: 95%">
                                    <div style="padding: 5px;" class="panel-title">
                                         <i style="font-size: x-large; float:left" class="fa fa-handshake"></i><span style="font-weight: bold;">{{row.Agenda}}</span>
                                        <i style="font-size: x-large; float:right" class="fa fa-angle-down"></i>
                                    </div>
                                    </div>
                                </div>
                        </a>
                    <div class="collapse{{row.Id}}" class="panel-collapse collapse">
                        <div style="background-color:snow" class="panel-body">
                            <div style="padding: 10px" class="row">
                                  <label class="label-big-check">
                                    <label for="{{row.Id}}" class="check-title"><i style="margin-right:10px" class="fa fa-check-circle"></i>Accepted</label>
                                    <label style="float:right"> {{row.AcceptAgenda}} Votes</label>
                                    <div class="progress">
                                        <div id="progressbar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{(row.AcceptAgenda*1)*100/(totalSubscribedshares*1)}}%;padding-left: 10px; color: rgb(51, 51, 0); background: rgb(204, 255, 204)">{{(row.AcceptAgenda*1)*100/(totalSubscribedshares*1)  | number : 2}}%
                                        </div>
                                    </div>
                                  </label>
                                  <br>
                                   <label class="label-big-check">
                                    <label for="{{row.Id}}" class="check-title"><i style="margin-right:10px" class="fa fa-minus-circle"></i>Declined</label>
                                    <label style="float:right"> {{row.DeclineAgenda}} Votes</label>
                                    <div class="progress">
                                        <div id="progressbar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{(row.DeclineAgenda*1)*100/(totalSubscribedshares*1)}}%;padding-left: 10px; color: rgb(51, 51, 0); background: rgb(255, 128, 128)">{{(row.DeclineAgenda*1)*100/(totalSubscribedshares*1)  | number : 2}}%
                                        </div>
                                    </div>
                                  </label>
                                  <br>
                                   <label class="label-big-check">
                                    <label for="{{row.Id}}" class="check-title"><i style="margin-right:10px" class="fa fa-exclamation-circle"></i>Abstain</label>
                                    <label style="float:right"> {{row.RS_OnAgenda}} Votes</label>
                                    <div class="progress">
                                        <div id="progressbar" class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:{{(row.RS_OnAgenda*1)*100/(totalSubscribedshares*1)}}%;padding-left: 10px; color: rgb(51, 51, 0); background: rgb(255, 224, 179)">{{(row.RS_OnAgenda*1)*100/(totalSubscribedshares*1) | number : 2}}%
                                        </div>
                                    </div>
                                  </label>
                            </div>
                        </div>
                    </div>
                </div>
  </div>

</div>
</div>


<div class="tab-pane fade in NoticeBoard">
    <h1> Notice/Results Announcement Board</h1>
    <div class="row" ng-repeat="row in notices">
        <div class="alert alert-info">
            <h3 style="font-weight: bolder;">{{row.noticeTitle}}</h3>
            <p style="white-space: pre-line; font-size:20px">{{row.noticeContent}}</p>
            <br>
        </div>
  </div>
</div>


    </div>
    </div>
    </div>
</div>






<script>
var RegisteredShareholdersNumber = 0;
var RegisteredShareholdersPercentage = 0;
var count = 0;
   var bar = new ProgressBar.Circle(container, {
  color: '#021',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 4,
  trailWidth: 1,
  easing: 'easeInOut',
  duration: 5400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#aaa', width: 2 },
  to: { color: '#333', width: 4 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = count;
    if (value === 0) {
      circle.setText('0');
    } else {
      circle.setText(value);
    }

  }
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '3rem';
 // Number from 0.0 to 1.0


 var bar2 = new ProgressBar.Circle(container2, {
  color: '#021',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 4,
  trailWidth: 1,
  easing: 'easeInOut',
  duration: 5400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#aaa', width: 2 },
  to: { color: '#333', width: 4 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = (RegisteredShareholdersPercentage * 100).toFixed(2);
    if (value === 0) {
      circle.setText('0 %');
    } else {
      circle.setText(value + ' %');
    }

  }
});
bar2.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar2.text.style.fontSize = '2rem';

liveUpdate();

function liveUpdate(){

xmlhttp = new XMLHttpRequest();
xmlhttp.open('GET', 'services/shareholdersCounter.php',false);
xmlhttp.send(null);

xmlhttp1 = new XMLHttpRequest();
xmlhttp1.open('GET', 'services/attendedShareholderssubscribedshare.php',false);
xmlhttp1.send(null);

xmlhttp2 = new XMLHttpRequest();
xmlhttp2.open('GET', 'services/TotalSubscribedshare.php',false);
xmlhttp2.send(null);

var json = JSON.parse(xmlhttp.responseText);
count = json.shareholders; // or json["Data"]

var json = JSON.parse(xmlhttp1.responseText);
var Attsubscribers = json.Attsubscribers;

var json2 = JSON.parse(xmlhttp2.responseText);
var totalsubscribedshare = json2.subscribers; // or json["Data"]

console.log(count+' '+totalsubscribedshare+' '+Attsubscribers);
RegisteredShareholdersNumber = ((((Attsubscribers*1)*100)/(totalsubscribedshare*1))/100);
RegisteredShareholdersPercentage = ((((Attsubscribers*1)*100)/(totalsubscribedshare*1))/100);
bar.animate(RegisteredShareholdersNumber); 
bar2.animate(RegisteredShareholdersPercentage);
};

setInterval(function(){
  liveUpdate();
},4000);

</script>
<script src="scripts/wow.js"></script>













           