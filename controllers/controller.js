/// <reference path="../scripts/angular.js" />

var app = angular.module("NibVoating", ["ngRoute", "angularUtils.directives.dirPagination"])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "views/home.php",
                 controller: "mainController"
            })
             .when("/addshareholder", {
                templateUrl: "views/newshareholder.htm",
                 controller: "shareholderController"
            })
              .when("/addcandidate", {
                templateUrl: "views/newcandidate.htm",
                controller: "candidateController"
            })
            .when("/voteregistration", {
                templateUrl: "views/submitVote.php",
                controller: "vaotesController"
            })
             .when("/voteapproval", {
                templateUrl: "views/voteApprover.php",
                controller: "vaoteApprovalController"
            })
            .when("/attendance", {
                templateUrl: "views/shareholdersAttendance.htm",
                controller: "attendanceController"
            })
             .when("/attendanceapproval", {
                templateUrl: "views/attendanceapproval.htm",
                controller: "attendanceapprovalController"
            })
            .when("/usercontrol", {
                templateUrl: "views/usersmanager.htm",
                controller: "usersController"
            })
            .when("/agendaControl", {
                templateUrl: "views/addagenda.htm",
                controller: "agendaController"
            })
            .when("/noticeControl", {
                templateUrl: "views/addnotice.htm",
                controller: "agendaController"
            })
            .when("/agendaVote", {
                templateUrl: "views/voteForAgenda.htm",
                controller: "agendaVoteController"
            })
            .when("/votereverse", {
                templateUrl: "views/reversevote.php",
                controller: "voteReversController"
            })
             .when("/agendavotereverse", {
                templateUrl: "views/agendareversevote.htm",
                controller: "AgendavoteReversController"
            })
              .when("/proxy", {
                templateUrl: "views/assignproxy.htm",
                controller: "proxyController"
            }) 
            .when("/BoardVoteReport", {
                templateUrl: "views/boardVoteReport.htm",
                controller: "reportsController"
            })
            .when("/AgendaVoteReport", {
                templateUrl: "views/agendaVoteReport.htm",
                controller: "reportsController"
            })
            .when("/ProxyReport", {
                templateUrl: "views/proxyReport.htm",
                controller: "reportsController"
            })
             .when("/ReversedVoteReport", { 
                templateUrl: "views/reversedVoteReport.htm",
                controller: "reportsController"
            })
              .when("/ShareholdersAttendance", { 
                templateUrl: "views/ShareholdersAttendanceReport.htm",
                controller: "reportsController"
            })
            .when("/changePassword", {
                templateUrl: "views/changePassword.php",
                controller: "usersController"
            })
    });

   app.run(['$http', function ($http) { 
      /*$http.post('services/shareholdersCounter.php', {tableName: 'shareholders'})
        .then(function (res) {
             console.log(res.data[0].shareholders);
            sessionStorage.RegisteredShareholdersNumber = (res.data[0].shareholders*1)/100;
            sessionStorage.RegisteredShareholdersPercentage = (((res.data[0].shareholders*1)*100)/20)/100;
      });*/
   }]);




          app.factory('ShareholdersService', function ($q, $http) {
              return {
                  getData: function () {
                      var q = $q.defer();
                      $http.post('services/fetchdata.php', {
                          tableName: 'shareholders'
                      }).then(function (response) {
                          q.resolve(response);
                      }, function (error) {
                          q.reject();
                      })
                      return q.promise;
                  },
                  saveData: function () {
                      return this.getData();
                  }
              }
          });


          app.factory('AttendedShareholdersReportService', function ($q, $http) {
              return {
                  getData: function () {
                      var q = $q.defer();
                      $http.post('services/fetchAttendedShareholdersForReport.php', {
                          tableName: 'shareholders'
                      }).then(function (response) {
                          q.resolve(response);
                      }, function (error) {
                          q.reject();
                      })
                      return q.promise;
                  },
                  saveData: function () {
                      return this.getData();
                  }
              }
          });

          app.factory('AttendedShareholdersService', function ($q, $http) {
              return {
                  getData: function () {
                      var q = $q.defer();
                      $http.post('services/fetchAttendedshareholders.php', {
                          tableName: 'shareholders'
                      }).then(function (response) {
                          q.resolve(response);
                      }, function (error) {
                          q.reject();
                      })
                      return q.promise;
                  },
                  saveData: function () {
                      return this.getData();
                  }
              }
          });

          app.factory('UnAttendedShareholdersService', function ($q, $http) {
              return {
                  getData: function () {
                      var q = $q.defer();
                      $http.post('services/fetchUnattendedshareholders.php', {
                          tableName: 'shareholders'
                      }).then(function (response) {
                          q.resolve(response);
                      }, function (error) {
                          q.reject();
                      })
                      return q.promise;
                  },
                  saveData: function () {
                      return this.getData();
                  }
              }
          });

          app.factory('AttendedShareholdersToBeApprovedService', function ($q, $http) {
              return {
                  getData: function () {
                      var q = $q.defer();
                      $http.post('services/fetchattendedshareholderstoBeApproved.php', {
                          tableName: 'shareholders'
                      }).then(function (response) {
                          q.resolve(response);
                      }, function (error) {
                          q.reject();
                      })
                      return q.promise;
                  },
                  saveData: function () {
                      return this.getData();
                  }
              }
          });

          app.factory('UnAttendedShareholdersByApprovalService', function ($q, $http) {
              return {
                  getData: function () {
                      var q = $q.defer();
                      $http.post('services/fetchUnattendedshareholdersbyApproval.php', {
                          tableName: 'shareholders'
                      }).then(function (response) {
                          q.resolve(response);
                      }, function (error) {
                          q.reject();
                      })
                      return q.promise;
                  },
                  saveData: function () {
                      return this.getData();
                  }
              }
          });


    app.controller('mainController', function ($scope, $http, $window) {
      
      $http.post('services/voteResult.php')
        .then(function (res) {
        $scope.candidates = res.data;
        $scope.candidateNumber = res.data.length;
      });

      $http.post('services/fetchdata.php', {tableName: 'agenda'})
        .then(function (res) {
           $scope.agendas = res.data;
      }); 

    $http.post('services/fetchAgendaReadyRoBeVoted.php', {tableName: 'agenda'})
        .then(function (res) {
           $scope.agendasVote = res.data;
    }); 

    $http.post('services/fetchdata.php', {tableName: 'Notice'})
    .then(function (res) {
      $scope.notices = res.data;
    });

      $http.post('services/TotalSubscribedshare.php')
        .then(function (res) {
           $scope.totalSubscribedshares = res.data.subscribers;
      });

      function refreshVotes(){
      
       $http.post('services/voteResult.php')
        .then(function (res) {
        console.log(res.data);
        $scope.candidates = res.data;
      
       $http.post('services/fetchdata.php', {tableName: 'agenda'})
        .then(function (res) {
           $scope.agendas = res.data;
       }); 

        $http.post('services/fetchAgendaReadyRoBeVoted.php', {tableName: 'agenda'})
         .then(function (res) {
        $scope.agendasVote = res.data;
    }); 

      });

      $http.post('services/getTotalSubscribedshare.php')
        .then(function (res) {
        $scope.votes = res.data[0].total;
        console.log(res.data);
      });

      $scope.makeDefaultToVote = function(Id){         
        $http.post('services/makeAgendaDefaultToVote.php',{id: Id})
        .success(function (data) {
            swal("Success", data , "success");
        });
       };
      };

    setInterval(function(){
        refreshVotes();
    },4000);

    });


    app.controller('shareholderController', function ($scope, $http, $window, ShareholdersService) { 

    $scope.IndluentialShareholders = [];
        
    ShareholdersService.saveData().then(function (res) {
        $scope.shareholders = res.data;
    });

    ShareholdersService.saveData()
    .then(function (res) {
           res.data.forEach(function(shareholder){
            var shareNumber = shareholder.numberOfshares * 1;
            var totalsubshare = $scope.totalSubscribedshares * 1;
            var shareholderSubPercentage = (shareNumber * 100) / totalsubshare;
            if(shareholderSubPercentage > 2)
            { 
              $scope.IndluentialShareholders.push(shareholder);
            }
           })
    });

    function refreshShareholders() {
        ShareholdersService.saveData().then(function (res) {
        $scope.shareholders = res.data;
        });
    }; 

    $http.post('services/TotalSubscribedshare.php')
    .then(function (res) {
       $scope.totalSubscribedshares = res.data.subscribers;
    });

     $scope.deleteShareholder = function (Id) {
        swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "img/delete.png",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $http.post('services/deleteShareholder.php', {
                        tableName: 'shareholders',
                        id: Id
                    }).then(function (response) {
                        refreshShareholders();
                        console.log(response.data);
                        swal("Success", "Shareholder Deleted Successfully!" , "success");
                    });
          };
        });
     };

    $scope.SaveParsedCSVtoDB = function () {
        if (sessionStorage.parsedRows != null) {
            var shareholders = sessionStorage.parsedRows;
            console.log(JSON.parse(shareholders));

            $http({
                method: 'POST',
                url: "services/saveParcedCSV.php",
                processData: false,
                data: JSON.parse(shareholders),
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                console.log(data);
                  swal({
                    title: "Success",
                    text: "Shareholders Saved Successfully!",
                    icon: "success",
                    button: false,
                    timer:2000
                })
                .then((willVote) => {
                   $window.location.reload();
                });
            });

        }
    };

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    };

    });




     app.controller('candidateController', function ($scope, $http, $window, ShareholdersService) {
      
      $http.post('services/fetchdata.php', {tableName: 'candidates'})
        .then(function (res) {
           $scope.candidates = res.data;
      });

      ShareholdersService.saveData().then(function (res) {
        $scope.shareholders = res.data;
      });

      function refreshCandidates() {
        $http.post('services/fetchdata.php', {tableName: 'candidates'})
        .then(function (res) {
           $scope.candidates = res.data;
        });
      };

      $scope.addcandidate = function () {
        var shareholdId = document.getElementById("ShareholderId").value;

        $http.post('services/fetchsinglecandidate.php', {
            tableName: 'candidates',
            id: shareholdId
        }).then(function (response) {
            if(response.data.length <= 0){
            $http({
            method: 'POST',
            url: "services/addcandidate.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("ShareholderId", document.getElementById("ShareholderId").value);
                formData.append("candidatename", document.getElementById('shareholderName').value);
                formData.append("NumberOfShare", document.getElementById("NumberOfShare").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            document.getElementById('shareholderName').value = "";
            document.getElementById("NumberOfShare").value = "";
            document.getElementById("candidatename").value = "";
            document.getElementById("candidatename").selectedIndex = "0";
            document.getElementById('amount').value = "";
            swal("Success", data , "success");
            refreshCandidates();
        });
        }
        else {
        swal({
          title: "Notice",
          text: "This Shareholder is already registered as candidate!",
          icon: "warning",
          dangerMode: true,
        });
        }
        });
      };

       $scope.deleteCandidate = function (Id) {
         swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "img/delete.png",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $http.post('services/delete.php', {
                tableName: 'candidates',
                id: Id
            }).then(function (response) {
                refreshCandidates();
                console.log(response.data);
                 swal("Success", "Candidate Deleted Successfully!" , "success");
            });
          };
        });
        };

        $scope.setCandidate = function (sharenumber) {
        var CanID = sharenumber.Id;
        $http.post('services/fetchsinglerow.php', {
            tableName: 'shareholders',
            id: CanID
        }).then(function (response) {
            document.getElementById('ShareholderId').value = response.data[0].Id;
            document.getElementById('shareholderName').value = response.data[0].Name;
            document.getElementById('NumberOfShare').value = response.data[0].numberOfshares;
            document.getElementById('amount').value = response.data[0].amount;
            console.log(response.data);
        });
    };
    });


  app.controller('vaotesController', function ($scope, $http, $window, AttendedShareholdersService) {

    $scope.checkedNumber = 0;

    var username = document.getElementById("username").value;

    AttendedShareholdersService.saveData().then(function (res) {
        $scope.shareholders = res.data;
    });

    $http.post('services/TotalSubscribedshare.php')
    .then(function (res) {
       $scope.totalSubscribedshares = res.data.subscribers;
    });

    $http.post('services/fetchdata.php', {tableName: 'candidates'})
        .then(function (res) {
           $scope.candidates = res.data;
           $scope.HalfCandidates = res.data.length/2;
    });

    $http.post('services/fetchdata.php', {tableName: 'MaxVote'})
        .then(function (res) {
           $scope.maxVote = res.data[0].max_Vote;
           $scope.limitNumber = res.data[0].max_Vote * 1;
    });

    $http.post('services/fetchdata.php', {tableName: 'voteType'})
    .then(function (res) {
        document.getElementById("VoterType").checked = res.data[0].non_influential_only * 1;
        $scope.votetype = res.data[0].non_influential_only * 1;
    });

    $scope.GetSelectedvalues = function () {
        var voaterId = $scope.voaterId.Id;

        $http.post('services/fetchVoterFromWaiting.php', {
            tableName: 'votestobeapproved',
            voaterid: voaterId
        }).then(function (response) {
            if(response.data.length <= 0){
               submitVoteTobeApproved();
            }
            else{
                swal({
                  title: "Notice",
                  text: "Vote of Shareholder with Id "+ voaterId +" is already Submited!",
                  icon: "warning",
                  dangerMode: true,
                });
                }
        });

        function submitVoteTobeApproved(){
        swal({
          title: "Are you sure?",
          text: "Once vote is submited, you will not be able to roll it back!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willVote) => {
          if (willVote) {
             if($scope.votetype === 1){
             $http.post('services/fetchVoterById.php', {voaterid: voaterId})
                 .then(function (res) {
                    var shareNumber = res.data[0].numberOfshares * 1;
                    var votername = res.data[0].Name;
                    var totalsubshare = $scope.totalSubscribedshares * 1;
                    var shareholderSubPercentage = (shareNumber * 100) / totalsubshare;
                    if(shareholderSubPercentage < 2)
                    {
                    var VotedcandidatesIds = "";
                    var Votedcandidates = "";
                    for (var i = 0; i < $scope.candidates.length; i++) {
                      if ($scope.candidates[i].checked) {
                        Votedcandidates += $scope.candidates[i].Name + ' , ';
                        VotedcandidatesIds += $scope.candidates[i].Id + ' , ';
                      }
                   };
                   $http.post('services/addTowaiting.php', {VoaterId: voaterId , votername: votername, votedcandidatesIds: VotedcandidatesIds, votedcandidates: Votedcandidates , ShareNumber: shareNumber , Username: username})
                          .success(function (data) {
                            swal({
                                title: "Success",
                                text: data,
                                icon: "success",
                                button: false,
                                timer:1500
                            })
                            .then((willVote) => {
                                $window.location.href = '#/voteregistration';
                                $window.location.reload();
                            });
                    });
                  }
                  else{
                    swal({
                        title: "Notice",
                        text: "Influential Shareholder Can't Vote For Now!",
                        icon: "warning",
                        dangerMode: true,
                      });
                  }
                });
            }
            else{
                $http.post('services/fetchVoterById.php', {voaterid: voaterId})
                 .then(function (res) {
                    var shareNumber = res.data[0].numberOfshares;
                    var votername = res.data[0].Name;
                    var VotedcandidatesIds = "";
                    var Votedcandidates = "";
                    for (var i = 0; i < $scope.candidates.length; i++) {
                      if ($scope.candidates[i].checked) {
                        Votedcandidates += $scope.candidates[i].Name + ' , ';
                        VotedcandidatesIds += $scope.candidates[i].Id + ' , ';
                      }
                   };
                   $http.post('services/addTowaiting.php', {VoaterId: voaterId , votername: votername, votedcandidatesIds: VotedcandidatesIds, votedcandidates: Votedcandidates , ShareNumber: shareNumber, Username: username})
                          .success(function (data) {
                            swal({
                                title: "Success",
                                text: data,
                                icon: "success",
                                button: false,
                                timer:1500
                            })
                            .then((willVote) => {
                                 $window.location.href = '#/voteregistration';
                                 $window.location.reload();
                            });
                    });
                });
            }
          };
        });
        };
       };

       $scope.setValues = function(shareholder){
        var Id = shareholder.Id;
        $http.post('services/fetchsinglerow.php', {
            tableName: 'shareholders',
            id: Id
        }).then(function (response) {
             $scope.voterId = response.data[0].Id;
             $scope.voterName = response.data[0].Name;
             $scope.noofshareholder = response.data[0].numberOfshares;
             $scope.amount = response.data[0].amount;
            console.log(response.data);
        });
       };
     
       $scope.setMaxVote = function(maxvote){
        $scope.limitNumber = maxvote * 1;

          $http.post('services/setMaxVote.php', {MaxVote: $scope.limitNumber})
          .success(function (data) {
        console.log(data);          
           swal({
            title:"Success",
            text: "Maximum number that can be voted by shareholders set to " + maxvote,
            icon: "success",
            dangerMode: false,
          });
          });
       };

       $scope.setVoteType = function(item){
        if (item.checked) {
            $http.post('services/setVoteType.php', {NonInfOnly: 1})
            .success(function (data) {
               console.log(data);
               swal({
                title:"Success",
                text: "Vote Type is Set To 'Non-Influentials' Only!",
                icon: "success",
                dangerMode: false,
              });
            });
            $scope.votetype = 1;
          }
        else if(!item.checked) {
            $http.post('services/setVoteType.php', {NonInfOnly: 0})
            .success(function (data) {
               console.log(data);
                swal({
                title:"Success",
                text: "Vote Type is Set To 'All' !",
                icon: "success",
                dangerMode: true,
              });
            });
            $scope.votetype = 0;
          }
       };

          $scope.check = function(item) {         
            if (item.checked) {
              $scope.checkedNumber++;
            } else {
              $scope.checkedNumber--;
            }

            if($scope.checkedNumber === $scope.limitNumber){
              /*swal({
                title: "Notice",
                text: "You Checked The Maximum Candidates Number!",
                icon: "warning",
                dangerMode: true,
              });*/
            };

          };

           $scope.checkMinimum = function(){
            if($scope.checkedNumber <= 0){
              swal({
                title: "Notice",
                text: "Select Atleast One Candidate!",
                icon: "warning",
                dangerMode: true,
              });
            };
           };

    });
     
       app.controller('vaoteApprovalController', function ($scope, $http, $window) { 

            var username = document.getElementById("username").value;

            $scope.myFilter = function (word) {
                if ($scope.search === '') return word.VoterId;
                return word.VoterId === $scope.search;
            }; 

            $http.post('services/fetchUnapprovedVotes.php', {tableName: 'votestobeapproved'})
               .then(function (res) {
               $scope.votes = res.data;
             });

             setInterval(function(){
                fetchwaitingvotes(); 
              },3000);

              $http.post('services/getMaker.php', {tableName: 'usermap', checker: username})
              .then(function (res) {
                $scope.maker = res.data[0].maker;
              });

              $scope.FilterMaker = function (_maker){
                if ($scope.maker === '') return _maker.MakerName;
                return _maker.MakerName === $scope.maker;
              }

             function fetchwaitingvotes() {
                $http.post('services/fetchUnapprovedVotes.php', {tableName: 'votestobeapproved'})
                    .then(function (res) {
                    $scope.votes = res.data;
             });
            }; 

            $scope.approvevote = function(Id){
              var username = document.getElementById("username").innerHTML;
                  swal({
                  title: "Are you sure?",
                  text: "Once vote is Approved, you will not be able to roll it back!",
                  icon: "warning",
                  buttons: true,
                  dangerMode: false,
                })
                .then((willVote) => {
                  if (willVote) {
                                $http.post('services/fetchsinglevote.php', {id: Id})
                                 .then(function (res) {
                                 var voterId = res.data[0].VoterId; 
                                 var voterName = res.data[0].VoterName; 
                                 var data = res.data[0].VotedCandidatesId;
                                 var VotedCandidatesId = res.data[0].VotedCandidatesId;
                                 var votedcandidates = res.data[0].VotedCandidates;
                                 var sharenumber = res.data[0].NumberOfShare;
                                 var candidateIds = data.split(","); 
                                 var leng = candidateIds.length - 1;

                                 for (var i = 0; i < leng; i++) {
                                        var candidateId = candidateIds[i];
                                          $http.post('services/submitVote.php', {candidateid: candidateId, Sharenumber: sharenumber})
                                          .success(function (data) {
                                             console.log(data);
                                          });
                                  };
                                   $http.post('services/saveVoteToList.php', {voterId: voterId, voterName: voterName, VotedCandidatesId: VotedCandidatesId, votedcandidates: votedcandidates, Sharenumber: sharenumber,encode: username})
                                          .success(function (data) {
                                             console.log(data);
                                  });
                                  UpdateApprovalState(Id); 
                                  swal({
                                    title: "Success",
                                    text: "Vote Approved Successfully!",
                                    icon: "success",
                                    button: false,
                                    timer:1500
                                  });
                             });
                          };
                        });
            };

            $scope.deletevote = function (Id) {
                 swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this data!",
                  icon: "img/delete.png",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $http.post('services/delete.php', {
                    tableName: 'votestobeapproved',
                    id: Id
                    }).then(function (response) {
                        fetchwaitingvotes();
                        console.log(response.data);
                        swal({
                            title: "Success",
                            text: "Vote Discarded Successfully!",
                            icon: "success",
                            button: false,
                            timer:1500
                        });
                    });
                  };
                });
        };

        function UpdateApprovalState(Id) {
            $http.post('services/approved.php', {
                tableName: 'votestobeapproved',
                id: Id
            }).then(function (response) {
                fetchwaitingvotes();
                console.log(response.data);
            });
        }

       });

        app.filter('customSplitString', function() {
                return function(input) {
                var arr = input.split(',');
                return arr;
         };
       });


       app.controller('voteReversController', function ($scope, $http, $window) { 

        var username = document.getElementById("username").innerHTML;

        $http.post('services/fetchdata.php', {tableName: 'votelist'})
         .then(function (res) {
           $scope.votes = res.data;
         });

         $scope.reverse = function(Id){
                  swal({
                  title: "Are you sure?",
                  text: "You are reversing this shareholder vote.",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willVote) => {
                  if (willVote) {
                                $http.post('services/fetchvoteForreversal.php', {id: Id})
                                 .then(function (res) {
                                 var voterId = res.data[0].VoterId;
                                 var voterName = res.data[0].VoterName;
                                 var VotedCandidatesId = res.data[0].VotedCandidatesId;
                                 var data = res.data[0].VotedCandidatesId;
                                 var votedcandidates = res.data[0].VotedCandidatesName;
                                 var sharenumber = res.data[0].SubscribedShare;
                                 var candidateIds = data.split(","); 
                                 var leng = candidateIds.length - 1;

                                 for (var i = 0; i < leng; i++) {
                                        var candidateId = candidateIds[i];
                                          $http.post('services/reverseVote.php', {candidateid: candidateId, Sharenumber: sharenumber})
                                          .success(function (data) {
                                             console.log(data);
                                          });
                                  };

                                  $http.post('services/saveToReversedVote.php', {voterId: voterId, voterName: voterName, VotedCandidatesId: VotedCandidatesId, votedcandidates: votedcandidates, Sharenumber: sharenumber,encode: username})
                                      .success(function (data) {
                                       console.log(data);
                                  });

                                    $http.post('services/deleteVoteFromWaitingList.php', {
                                    tableName: 'votestobeapproved',
                                    id: voterId
                                    }).then(function (response) {
                                        console.log(response.data);
                                    });

                                   $http.post('services/delete.php', {
                                        tableName: 'votelist',
                                        id: Id
                                   }).then(function (response) {
                                      console.log(response.data);
                                   });
                             });
                                    swal({
                                        title: "Success",
                                        text: "Vote Reversed Successfully!",
                                        icon: "success",
                                        button: false,
                                        timer:1500
                                    })
                                    .then((willVote) => {
                                        $window.location.href = '#/votereverse';
                                        $window.location.reload();
                                });
                          };
                        });
         };

       });




      app.controller('AgendavoteReversController', function ($scope, $http, $window) {  

        $http.post('services/fetchdata.php', {tableName: 'votesonagenda'})
         .then(function (res) {
           $scope.votes = res.data;
         });

          $scope.reverse = function(Id){
            var declined = 0;
            var remainSilent = 0;

                  swal({
                  title: "Are you sure?",
                  text: "You are reversing this shareholder vote.",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willVote) => {
                  if (willVote) {

                      $http.post('services/fetchAgendavoteForreversal.php', {id: Id})
                      .then(function (res) { 
                              var voterId = res.data[0].VoterId;
                              var voterName = res.data[0].VoterName;
                              var subdcribrdShare = res.data[0].subdcribrdShare * 1;
                              var agendaId = res.data[0].agendaId;
                              var AgendaTopic = res.data[0].AgendaTopic;
                              var vote = res.data[0].vote;

                                 if(vote == "Declined"){
                                    declined = subdcribrdShare * 1;
                                 }
                                 else if(vote == "Abstain"){
                                    remainSilent = subdcribrdShare * 1;
                                 }

                               $http.post('services/reverseAgendaVote.php', {AgendaId: agendaId, Sharenumber: subdcribrdShare, declined: declined, remainSilent: remainSilent})
                                          .success(function (data) {
                                             console.log(data);
                               });

                               $http.post('services/SavereverseedAgendaVote.php', {voterId: voterId, voterName: voterName, subdcribrdShare: subdcribrdShare, agendaId: agendaId, AgendaTopic: AgendaTopic, vote: vote})
                                          .success(function (data) {
                                             console.log(data);
                               });

                               $http.post('services/delete.php', {
                                  tableName: 'votesonagenda',
                                  id: Id
                               });

                               swal({
                                      title: "Success",
                                      text: "Vote Reversed Successfully!",
                                      icon: "success"
                                    })
                                    .then((willVote) => {
                                        $window.location.href = '#/agendavotereverse';
                                        $window.location.reload();
                                });
                      });

                  }
                });
            };

      });



       app.controller('attendanceController', function ($scope, $http, $window, AttendedShareholdersService, UnAttendedShareholdersService) {
        
        var username = document.getElementById("username").value;
         
        $scope.myFilter = function (word) {
              if ($scope.search === '') return word.Id;
              return word.Id === $scope.search;
        }; 

          $scope.myFilter2 = function (word) {
              if ($scope.search2 === '') return word.Id;
              return word.Id === $scope.search2;
          }; 

          UnAttendedShareholdersService.saveData().then(function (res) {
            $scope.shareholders = res.data;
          });

          AttendedShareholdersService.saveData().then(function (res) {
            $scope.Attendedshareholders = res.data;
          });

          function refreshShareholders()
          {
          UnAttendedShareholdersService.saveData().then(function (res) {
            $scope.shareholders = res.data;
          });

          AttendedShareholdersService.saveData().then(function (res) {
            $scope.Attendedshareholders = res.data;
          });

         };

         $scope.checkattendance = function(Id){
                $http.post('services/fetchsingleshareholderFromProxy.php', {
                    tableName: 'proxy',
                    id: Id
                }).then(function (response) {
                  if(response.data.length > 0){
                  swal({
                   title: "Notice",
                   text: "This Shareholder is Assigned to some Proxy, Click 'Proxy' if the proxy exist or Click 'Not Proxy' if the shareholder exist.",
                   icon: "img/caoution.png",
                   buttons: {
                      cancel: "Cancel",
                      NotProxy: "Not Proxy",
                      Proxy: "Proxy",
                    },
                })
                .then((proxy) => {
                  if (proxy === 'Proxy') {
                      swal({
                        title: "Are you sure?",
                        text: "You are Saying, The Proxy of shareholder With Id "+ Id +" is Attended.",
                        icon: "img/caoution.png",
                        buttons: true
                      })
                      .then((willAttend) => {
                        if (willAttend) {
                           $http.post('services/waitingTobeattended.php', {shareholderId: Id , Username: username})
                           .success(function (data) {
                            swal({
                                title: "Success",
                                text: "Shareholder Attendance Added To Wating List To Be Approved Successfully!",
                                icon: "success",
                                button: false,
                                timer:1500
                            });
                            $scope.search = '';
                            refreshShareholders();
                          });
                        };
                      });
                  }
                  else if(proxy === 'NotProxy'){   
                      swal({
                        title: "Are you sure?",
                        text: "You are Saying, shareholder With Id "+ Id +" him/her self is Attended, Not the Proxy.",
                        icon: "img/caoution.png",
                        buttons: true
                      })
                      .then((willAttend) => {
                        if (willAttend) {
                           $http.post('services/updateProxy.php', {shareholderId: Id})
                           .success(function(data) {
                              console.log(data);
                           });
                           $http.post('services/waitingTobeattended.php', {shareholderId: Id, Username: username})
                           .success(function (data) {
                            swal({
                                title: "Success",
                                text: "Shareholder Attendance Added To Wating List To Be Approved Successfully!",
                                icon: "success",
                                button: false,
                                timer:1500
                            });
                           $scope.search = '';
                           refreshShareholders();
                          });
                        };
                      });
                  }
                  });
                  }
                  else{   
                      swal({
                        title: "Are you sure?",
                       text: "You are Saying, shareholder With Id "+ Id +" is attended.",
                        icon: "warning",
                        buttons: true
                      })
                      .then((willAttend) => {
                        if (willAttend) {
                           $http.post('services/waitingTobeattended.php', {shareholderId: Id,  Username: username})
                           .success(function (data) {
                            swal({
                                title: "Success",
                                text: "Shareholder Attendance Added To Wating List To Be Approved Successfully!",
                                icon: "success",
                                button: false,
                                timer:1500
                            });
                            $scope.search = '';
                           refreshShareholders();
                          });
                        };
                      });
                  }
              });
         };

          });



         app.controller('attendanceapprovalController', function ($scope, $http, $window, AttendedShareholdersToBeApprovedService, AttendedShareholdersService) {

          var username = document.getElementById("username").value;

          $http.post('services/getMaker.php', {tableName: 'usermap', checker: username})
          .then(function (res) {
            $scope.maker = res.data[0].maker;
          });

          $scope.myFilter = function (word) {
              if ($scope.search === '') return word.Id;
              return word.Id === $scope.search;
          }; 

          $scope.myFilter2 = function (word) {
              if ($scope.search2 === '') return word.Id;
              return word.Id === $scope.search2;
          }; 
          
          setInterval(function(){
            refreshShareholders();
          },5000);
          
        $http.post('services/fetchattendedshareholderstoBeApproved.php', {
        tableName: 'shareholders'
        }).then(function (res) {
            $scope.shareholders = res.data.filter(function(maker){
                return maker.MakerName === $scope.maker;
            })
        });

          AttendedShareholdersService.saveData().then(function (res) {
            $scope.Attendedshareholders = res.data;
          });

         function refreshShareholders(){
            $http.post('services/fetchattendedshareholderstoBeApproved.php', {
                tableName: 'shareholders'
                }).then(function (res) {
                    $scope.shareholders = res.data.filter(function(maker){
                        return maker.MakerName === $scope.maker;
                    })
            });

          AttendedShareholdersService.saveData().then(function (res) {
            $scope.Attendedshareholders = res.data;
          });
       };

         $scope.checkapprovedattendance = function(Id){
             swal({
                  title: "Are you sure?",
                  text: "You are Approving that shareholder With Id "+ Id +" is Attended.",
                  icon: "warning",
                  buttons: true
                })
                .then((willAttend) => {
                  if (willAttend) {
                     $http.post('services/attended.php', {shareholderId: Id , Username: username})
                     .success(function (data) {
                      swal({
                        title: "Success",
                        text: "Shareholder Attendance Approved Successfully!",
                        icon: "success",
                        button: false,
                        timer:1500
                    });
                     $scope.search = '';
                     refreshShareholders();
                    });
                  };
                });
         };

          $scope.unattend = function(Id){
              swal({
                  title: "Are you sure?",
                  text: "You are Saying, shareholder With Id "+ Id +" is not attended.",
                  icon: "warning",
                  buttons: true
                })
                .then((willAttend) => {
                  if (willAttend) {
                        $http.post('services/Unattended.php', {shareholderId: Id})
                        .success(function (data) {
                         swal({
                            title: "Success",
                            text: "Shareholder Attendance Roll Backed Successfully!",
                            icon: "success",
                            button: false,
                            timer:2000
                        });
                         refreshShareholders();
                        });
                  };
          });
         };

         $scope.SubmitAll = function(){
            swal({
                title: "Are you sure?",
                text: "You Want To Approve All Shareholders Attendance On Waiting List ?",
                icon: "warning",
                buttons: true
              })
              .then((willAttend) => {
                if (willAttend) {
                    $scope.shareholders.forEach(shareholder  => {
                    $http.post('services/attended.php', {shareholderId: shareholder.Id , Username: username})
                     .success(function (data) {
                        console.log("Shareholder Attended SuccessFully!");
                    });
                   });
                   swal({
                    title: "Success",
                    text: "Shareholders Attendance Approved Successfully!",
                    icon: "success",
                    button: false,
                    timer:1500
                });
                refreshShareholders();
                };
            });
         };

         });



         app.controller('usersController', function ($scope, $http, $window) {
         
         $http.post('services/fetchdata.php', {tableName: 'app_users'})
         .then(function (res) {
           $scope.users = res.data;
         });

         $http.post('services/fetchdata.php', {tableName: 'usermap'})
         .then(function (res) {
           $scope.mapedusers = res.data;
         });

         $http.post('services/fetchdata.php', {tableName: 'app_users'})
         .then(function (res) {
           $scope.Regmakers = res.data.filter(function (RegMakerUser) {
               return RegMakerUser.role == "Registrar";
             });
         });

         $http.post('services/fetchdata.php', {tableName: 'app_users'})
         .then(function (res) {
           $scope.Regcheckers = res.data.filter(function (RegCheckerUser) {
               return RegCheckerUser.role == "RegAuditor";
             });
         });

         $http.post('services/fetchdata.php', {tableName: 'app_users'})
         .then(function (res) {
           $scope.Votemakers = res.data.filter(function (VoteMakerUser) {
               return VoteMakerUser.role == "Voter";
             });
         });

         $http.post('services/fetchdata.php', {tableName: 'app_users'})
         .then(function (res) {
           $scope.Votecheckers = res.data.filter(function (VoteCheckerUser) {
               return VoteCheckerUser.role == "VoteAuditor";
             });
         });

         function refreshUsers(){
           $http.post('services/fetchdata.php', {tableName: 'app_users'})
            .then(function (res) {
            $scope.users = res.data;
            $scope.VoteCheckUser = res.data;
           });
         };

         function refreshMapedUsers(){
            $http.post('services/fetchdata.php', {tableName: 'usermap'})
            .then(function (res) {
              $scope.mapedusers = res.data;
            });
         };

         $scope.Adduser = function(){
            $http.post('services/fetchsingleUser.php', {tableName: 'app_users',username: document.getElementById("user_name").value})
            .then(function (res) {
              if(res.data.length > 0){
                swal({
                    title: "Notice",
                    text: "This Username is Already Exist!",
                    icon: "warning",
                    dangerMode: true,
                  });
              }
              else{
                $http({
                    method: 'POST',
                    url: "services/adduser.php",
                    processData: false,
                    transformRequest: function (data) {
                        var formData = new FormData();
                        formData.append("username", document.getElementById("user_name").value);
                        formData.append("password", document.getElementById("password").value);
                        formData.append("role", document.getElementById("role").value);
                        return formData;
                    },
                    data: $scope.form,
                    headers: {
                        'Content-Type': undefined
                    }
                }).success(function (data) {
                    refreshUsers();
                    document.getElementById("user_name").value = "";
                    document.getElementById("password").value = "";
                    document.getElementById("role").value = "";
                    swal("Success", data , "success");
                });
              }
            });
        };

        $scope.changePassword = function () {
        $http({
            method: 'POST',
            url: "services/changePassword.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("uname", document.getElementById("uname").value);
                formData.append("newpass", document.getElementById("newpass").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            swal('Success',data,'success');
            document.getElementById("oldpass").value = "";
            document.getElementById("newpass").value = "";
            document.getElementById("Cnewpass").value = "";
        });
       };
 
         $scope.deleteUser = function(Id){
            swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this data!",
                  icon: "img/delete.png",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $http.post('services/delete.php', {
                    tableName: 'app_users',
                    id: Id
                    }).then(function (response) {
                        refreshUsers();
                        console.log(response.data);
                        swal("Success", "User Removed Successfully!" , "success");
                    });
                  };
                });
         };

         $scope.deleteMap = function(Id){
            swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this data!",
                  icon: "img/delete.png",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $http.post('services/delete.php', {
                    tableName: 'usermap',
                    id: Id
                    }).then(function (response) {
                        refreshMapedUsers();
                        console.log(response.data);
                        swal("Success", "Users Map Removed Successfully!" , "success");
                    });
                  };
                });
         };

         $scope.MapRegUser = function(){
                $http({
                  method: 'POST',
                  url: "services/mapreguser.php",
                  processData: false,
                  transformRequest: function (data) {
                      var formData = new FormData();
                      formData.append("RegMaker", document.getElementById("RegMaker").value);
                      formData.append("RegChecker", document.getElementById("RegChecker").value);
                      return formData;
                  },
                  data: $scope.form,
                  headers: {
                      'Content-Type': undefined
                  }
              }).success(function (data) {
                swal("Success", data , "success");
                refreshMapedUsers();
              });
         };

         $scope.MapVoteUser = function(){
            $http({
              method: 'POST',
              url: "services/mapvoteuser.php",
              processData: false,
              transformRequest: function (data) {
                  var formData = new FormData();
                  formData.append("VoteReg", document.getElementById("VoteReg").value);
                  formData.append("VoteChk", document.getElementById("VoteChk").value);
                  return formData;
              },
              data: $scope.form,
              headers: {
                  'Content-Type': undefined
              }
          }).success(function (data) {
            swal("Success", data , "success");
            refreshMapedUsers();
          });
     };

         });


        app.controller('agendaController', function ($scope, $http, $window) {

         $http.post('services/fetchdata.php', {tableName: 'agenda'})
         .then(function (res) {
           $scope.agendas = res.data;
         });

         $http.post('services/fetchdata.php', {tableName: 'Notice'})
         .then(function (res) {
           $scope.notices = res.data;
         });

         function refreshAgendas(){
           $http.post('services/fetchdata.php', {tableName: 'agenda'})
         .then(function (res) {
           $scope.agendas = res.data;
         });
         };

         function refreshNotice(){
            $http.post('services/fetchdata.php', {tableName: 'Notice'})
            .then(function (res) {
              $scope.notices = res.data;
            });
         };

            $scope.addAgenda = function(){
                   $http.post('services/TotalSubscribedshare.php')
                  .then(function (res) {
                      var totalSubscribedshares = res.data.subscribers;        
                      $http({
                      method: 'POST',
                      url: "services/addAgenda.php",
                      processData: false,
                      transformRequest: function (data) {
                          var formData = new FormData();
                          formData.append("agenda", document.getElementById("agenda").value);
                          formData.append("agendaDesc", document.getElementById("agendaDesc").value);
                          formData.append("totalshares", totalSubscribedshares);
                          return formData;
                      },
                      data: $scope.form,
                      headers: {
                          'Content-Type': undefined
                      }
                      }).success(function (data) {
                      document.getElementById("agenda").value = "";
                      document.getElementById("agendaDesc").value = "";
                      swal("Success", data , "success");
                      refreshAgendas();
                      });
                     });
          };

          $scope.addNotice = function(){     
               $http({
               method: 'POST',
               url: "services/addNotice.php",
               processData: false,
               transformRequest: function (data) {
                   var formData = new FormData();
                   formData.append("notice", document.getElementById("notice").value);
                   formData.append("noticeCont", document.getElementById("noticeCont").value);
                   return formData;
               },
               data: $scope.form,
               headers: {
                   'Content-Type': undefined
               }
               }).success(function (data) {
               document.getElementById("notice").value = "";
               document.getElementById("noticeCont").value = "";
               swal("Success", data , "success");
               refreshNotice();
               });
         };


            $scope.deleteAgenda = function(Id){
            swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this data!",
                  icon: "img/delete.png",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $http.post('services/delete.php', {
                    tableName: 'agenda',
                    id: Id
                    }).then(function (response) {
                        refreshAgendas();
                        console.log(response.data);
                        swal("Success", "Agenda Removed Successfully!" , "success");
                    });
                  };
                });
            };

            $scope.deleteNotice = function(Id){
                swal({
                      title: "Are you sure?",
                      text: "Once deleted, you will not be able to recover this data!",
                      icon: "img/delete.png",
                      buttons: true,
                      dangerMode: true,
                    })
                    .then((willDelete) => {
                      if (willDelete) {
                        $http.post('services/delete.php', {
                        tableName: 'Notice',
                        id: Id
                        }).then(function (response) {
                            refreshNotice();
                            console.log(response.data);
                            swal("Success", "Notice Removed Successfully!" , "success");
                        });
                      };
                    });
                };

          });


         app.controller('agendaVoteController', function ($scope, $http, $window, AttendedShareholdersService) {
          
          $scope.groupOfDefaultRadios = 1;
          
            AttendedShareholdersService.saveData().then(function (res) {
               $scope.shareholders = res.data;
            });

            $http.post('services/fetchAgendaReadyRoBeVoted.php', {tableName: 'agenda'})
            .then(function (res) {
               $scope.agendas = res.data;
               $scope.agenda = res.data[0].Agenda;
               $scope.agendaId = res.data[0].Id;
               $scope.agendaTopic = res.data[0].Agenda;
               $scope.agendaDesc = res.data[0].AgendaDesc;
            });

            $scope.setValues = function(shareholder){
               var Id = shareholder.Id;
               $http.post('services/fetchsinglerow.php', {
               tableName: 'shareholders',
               id: Id
             }).then(function (response) {
             $scope.shareholderId = Id;
             $scope.voterName = response.data[0].Name;
             $scope.noofshareholder = response.data[0].numberOfshares;
             $scope.amount = response.data[0].amount;
             console.log(response.data);
             });
            };

           $scope.submitVote = function(selected){
              var voaterId = document.getElementById("VoterId").innerHTML;
              var agendaId = document.getElementById("agendaId").innerHTML;
              var voterName = document.getElementById("voterName").innerHTML;
              var agendaName = document.getElementById("agendaTopic").innerHTML;
              var declined = 0;
              var remainSilent = 0;
              var SubscribedShare = document.getElementById("NumberOfShare").innerHTML;


            $http.post('services/fetchvotersfromAgendaVotes.php', {
            tableName: 'votesonagenda',
            voaterid: voaterId,
            agendaid: agendaId
            }).then(function (response) {
             if(response.data.length <= 0){
               swal({
                      title: "Are you sure?",
                      text: "Once vote is submited, you will not be able to roll it back!",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    })
                    .then((willVote) => {
                      if (willVote) { 
                          voteForAgenda();
                      }
                    });
             }
             else{
                swal({
                  title: "Notice",
                  text: "Vote of Shareholder with This Id is already Submited!",
                  icon: "warning",
                  dangerMode: true,
                });
                }
             });

              function voteForAgenda(){
              var votevalue;
              $http.post('services/TotalSubscribedshare.php',{tableName: 'shareholders'})
                .then(function (res) {
                   var TotalSubscribedshare = res.data.subscribers;
                   if(selected*1 === 2){
                      declined = SubscribedShare * 1;
                      votevalue = "Declined";
                   }
                   else if(selected*1 === 3){
                      remainSilent = SubscribedShare * 1;
                      votevalue = "Abstain";
                   }
                   else {
                     votevalue = "Accepted";
                   }
                   var accepted = declined + remainSilent;
                    $http({
                        method: 'POST',
                        url: "services/addVoteForAgenda.php",
                        processData: false,
                        transformRequest: function (data) {
                            var formData = new FormData();
                            formData.append("Id", agendaId);
                            formData.append("declined", declined);
                            formData.append("remainSilent", remainSilent);
                            formData.append("accepted", accepted);
                            formData.append("voaterId", voaterId);
                            formData.append("voterName", voterName);
                            formData.append("SubscribedShare",SubscribedShare);
                            formData.append("agendaName", agendaName);
                            formData.append("votevalue",votevalue);
                            return formData;
                        },
                        data: $scope.form,
                        headers: {
                            'Content-Type': undefined
                        }
                        }).success(function (data) {
                           swal({
                                  title: "Success",
                                  text: data,
                                  icon: "success"
                                })
                                  .then((willVote) => {
                                  $window.location.reload();
                                });
                        });
                    });
              };
           };
         });


         app.controller('proxyController', function ($scope, $http, $window,  ShareholdersService) { 
              
              ShareholdersService.saveData().then(function (res) {
                 $scope.shareholders = res.data;
              });

               $http.post('services/fetchValidProxy.php', {tableName: 'proxy'})
                 .then(function (res) {
               $scope.proxys = res.data;
               });

                  function refreshProxys(){
                     $http.post('services/fetchValidProxy.php', {tableName: 'proxy'})
                    .then(function (res) {
                     $scope.proxys = res.data;
                     });
                  };

                $scope.addProxy = function()
                {
                var selectShareholders = String($("#shareholderSelector").val());
                var array = selectShareholders.split(',');
                array.forEach(function (entry) {

                $http.post('services/fetchsingleshareholderFromProxy.php', {
                    tableName: 'proxy',
                    id: entry
                }).then(function (response) {
                  if(response.data.length > 0){
                     swal({
                        title: "Notice",
                        text: "Proxy Assigned Shareholder Selected, Check It Again!",
                        icon: "warning",
                        dangerMode: true,
                });
                  }
                  else{
                    $http.post('services/fetchsinglerow.php', {
                    tableName: 'shareholders',
                    id: entry
                }).then(function (response) {
                    var proxyName = $scope.proxyName;
                    var shareholderId = entry*1;
                    var shareholderName = response.data[0].Name;
                    var subscribedshare = response.data[0].numberOfshares;
                    var amount = response.data[0].amount;
                    $http({
                        method: 'POST',
                        url: "services/addproxy.php",
                        processData: false,
                        transformRequest: function (data) {
                            var formData = new FormData();
                            formData.append("proxyName", proxyName);
                            formData.append("shareholderId", shareholderId);
                            formData.append("shareholderName", shareholderName);
                            formData.append("subscribedshare", subscribedshare);
                            formData.append("amount", amount);
                            return formData;
                        },
                        data: $scope.form,
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(function (data) {
                        console.log(data);
                    });
                });
                           swal({
                              title: "Success",
                              text: "Proxy Assigned Successfully!",
                              icon: "success"
                            })
                            .then((willVote) => {
                                $window.location.href = '#/proxy';
                                $window.location.reload();
                            });
                }
                });

             });
             };

             $scope.deleteProxy = function(Id){
                swal({
                  title: "Are you sure?",
                  text: "Once deleted, you will not be able to recover this data!",
                  icon: "img/delete.png",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $http.post('services/delete.php', {
                    tableName: 'proxy',
                    id: Id
                    }).then(function (response) {
                        console.log(response.data);
                        swal("Success", "Proxy Removed Successfully!" , "success");
                        refreshProxys();
                    });
                  };
                });
             };
         });


         app.controller('reportsController', function ($scope, $http, $window, AttendedShareholdersReportService, UnAttendedShareholdersByApprovalService) { 
        
         $scope.limiter = 200;
         $scope.limiter2 = 200;

         $http.post('services/TotalSubscribedshare.php')
         .then(function (res) {
            $scope.totalSubscribedshares = res.data.subscribers;
         });
     
         $http.post('services/fetchdata.php', {tableName: 'votelist'})
         .then(function (res) {
           $scope.votes = res.data;
           $scope.TotalVoteshareNumber = 0;
            res.data.forEach(function(shareholder){
                $scope.totalVoteShareHolders = res.data.length;
                $scope.TotalVoteshareNumber = $scope.TotalVoteshareNumber + shareholder.SubscribedShare * 1;
                $scope.Voterspercentage = ($scope.TotalVoteshareNumber* 100) / $scope.totalSubscribedshares * 1;
               })
         });

         $http.post('services/fetchdata.php', {tableName: 'agenda'})
         .then(function (res) {
           $scope.agendas = res.data;
         });

         $http.post('services/fetchdata.php', {tableName: 'votesonagenda'})
         .then(function (res) {
           $scope.votesonAgenda = res.data;
         });

         $http.post('services/fetchdata.php', {tableName: 'reversedvotes'})
         .then(function (res) {
           $scope.Reversedvotes = res.data;
         });


         $http.post('services/fetchdata.php', {tableName: 'reversedvoteonagenda'})
         .then(function (res) {
           $scope.ReversedAgendavotes = res.data;
         });
 
         $http.post('services/fetchValidProxy.php', {tableName: 'proxy'})
            .then(function (res) {
            $scope.proxys = res.data;
            $scope.TotalProxshareNumber = 0;
            $scope.TotalProxAmountBirr = 0;
            res.data.forEach(function(shareholder){
                $scope.totalproxy = res.data.length;
                $scope.TotalProxshareNumber = $scope.TotalProxshareNumber + shareholder.subscribedshare * 1;
                $scope.TotalProxAmountBirr = $scope.TotalProxAmountBirr + shareholder.amount * 1;
               });
         });

          $http.post('services/fetchInValidProxy.php', {tableName: 'proxy'})
            .then(function (res) {
            $scope.Inproxys = res.data;
         });

          AttendedShareholdersReportService.saveData().then(function (res) {
            $scope.attendedShareholders = res.data;
            $scope.TotalshareNumber = 0;
            $scope.TotalAmountBirr = 0;
            res.data.forEach(function(shareholder){
                $scope.totalAttendedShareHolders = res.data.length;
                $scope.TotalshareNumber = $scope.TotalshareNumber + shareholder.numberOfshares * 1;
                $scope.TotalAmountBirr = $scope.TotalAmountBirr + shareholder.amount * 1;
                $scope.percentage = ($scope.TotalshareNumber * 100) / $scope.totalSubscribedshares * 1;
               })
          });

          UnAttendedShareholdersByApprovalService.saveData().then(function (res) {
            $scope.UnattendedShareholders = res.data;
            $scope.TotalshareNumber2 = 0;
            $scope.TotalAmountBirr2 = 0;
            res.data.forEach(function(shareholder){
                $scope.totalAttendedShareHolders2 = res.data.length;
                $scope.TotalshareNumber2 = $scope.TotalshareNumber2 + shareholder.numberOfshares * 1;
                $scope.TotalAmountBirr2 = $scope.TotalAmountBirr2 + shareholder.amount * 1;
                $scope.percentage2 = ($scope.TotalshareNumber2 * 100) / $scope.totalSubscribedshares * 1;
               })
          });

         });

        function executepersecond(x) {
          return new Promise(resolve => {
            setTimeout(() => {
              resolve(x * 2);
            }, 1000);
          });
        }




            function handleFile(e) {
            //Get the files from Upload control
            var files = e.target.files;
            var i, f;
            var rows = [];
            //Loop through files
            for (i = 0, f = files[i]; i != files.length; ++i) {
                var reader = new FileReader();
                var name = f.name;
                reader.onload = function (e) {
                    var data = e.target.result;

                    var result;
                    var workbook = XLSX.read(data, { type: 'binary' });

                    var sheet_name_list = workbook.SheetNames;
                    sheet_name_list.forEach(function (y) { /* iterate through sheets */
                        //Convert the cell value to Json
                        var roa = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                        if (roa.length > 0) {
                            result = roa;
                        }
                    });
                    //Get the first column first cell value
                    sessionStorage.parsedRows = JSON.stringify(result);
                    console.log(result);
                    for (let x = 0; x < result.length; x++) {
                        var Id = result[x].Id;
                        var Name = result[x].Name;
                        var SubscribedShare = result[x].numberOfshares;
                        var amount = result[x].amount;
                        var table = document.getElementById('table');
                        var tbody = document.createElement('tbody');
                        var tr = document.createElement('tr');
                        tr.innerHTML = '<td>' + [Id, Name, SubscribedShare, amount].join('</td><td>') + '</td>';
                        tbody.appendChild(tr);
                        table.appendChild(tbody);
                        //alert(result[0].Name);
                    }
                };
                reader.readAsArrayBuffer(f);
            }
            document.getElementById('exportBtn').disabled = false;
        };






//To Excel Exporters
var xport = {
    _fallbacktoCSV: true,
    toXLS: function (tableId, filename) {
        this._filename = (typeof filename == 'undefined') ? tableId : filename;

        //var ieVersion = this._getMsieVersion();
        //Fallback to CSV for IE & Edge
        if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
            return this.toCSV(tableId);
        } else if (this._getMsieVersion() || this._isFirefox()) {
            alert("Not supported browser");
        }

        //Other Browser can download xls
        var htmltable = document.getElementById(tableId);
        var html = htmltable.outerHTML;

        this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
    },
    toCSV: function (tableId, filename) {
        this._filename = (typeof filename === 'undefined') ? tableId : filename;
        // Generate our CSV string from out HTML Table
        var csv = this._tableToCSV(document.getElementById(tableId));
        // Create a CSV Blob
        var blob = new Blob([csv], {
            type: "text/csv"
        });

        // Determine which approach to take for the download
        if (navigator.msSaveOrOpenBlob) {
            // Works for Internet Explorer and Microsoft Edge
            navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
        } else {
            this._downloadAnchor(URL.createObjectURL(blob), 'csv');
        }
    },
    _getMsieVersion: function () {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf("MSIE ");
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
        }

        var trident = ua.indexOf("Trident/");
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf("rv:");
            return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
        }

        var edge = ua.indexOf("Edge/");
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
        }

        // other browser
        return false;
    },
    _isFirefox: function () {
        if (navigator.userAgent.indexOf("Firefox") > 0) {
            return 1;
        }

        return 0;
    },
    _downloadAnchor: function (content, ext) {
        var anchor = document.createElement("a");
        anchor.style = "display:none !important";
        anchor.id = "downloadanchor";
        document.body.appendChild(anchor);

        // If the [download] attribute is supported, try to use it

        if ("download" in anchor) {
            anchor.download = this._filename + "." + ext;
        }
        anchor.href = content;
        anchor.click();
        anchor.remove();
    },
    _tableToCSV: function (table) {
        // We'll be co-opting `slice` to create arrays
        var slice = Array.prototype.slice;

        return slice
            .call(table.rows)
            .map(function (row) {
                return slice
                    .call(row.cells)
                    .map(function (cell) {
                        return '"t"'.replace("t", cell.textContent);
                    })
                    .join(",");
            })
            .join("\r\n");
    }
};
//End

var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}



     /*     var voaterId = $scope.voaterId;
                $http.post('services/fetchVoterById.php', {voaterid: voaterId})
                 .then(function (res) {
                    var shareNumber = res.data[0].numberOfshares;
                     for (var i = 0; i < $scope.candidates.length; i++) {
                      if ($scope.candidates[i].Selected) {
                        var candidateId = $scope.candidates[i].Id;
                        var candidatevote = $scope.candidates[i].votes;
                        candidatevote = (candidatevote*1) + (shareNumber*1);
                          $http.post('services/submitVote.php', {candidateid: candidateId, candidateVote: candidatevote})
                          .success(function (data) {
                             console.log(data);
                          });
                      }
                }
                alert("Vote Submited Successfully!");
                $window.location.href = '#/voteregistration';
                $window.location.reload();
                });*/