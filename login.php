<?php session_start(); $session_value = (isset($_SESSION['error']))?$_SESSION['error']:''; ?>
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/font-awesome.css">
<link rel="stylesheet" href="assets/css/fontawesome-all.css">
<link rel="shortcut icon" href="img/NIBLogo.png" type="image/x-icon">


<div style="background-image:url('img/back1.jpg'); height:100vh; width:100vw; background-size: 100% 100%;" class="container-fluid">
<div style="margin-top: 10%; min-width: 200px; width: 450px; align-items: center; opacity:0.95; background-color:rgb(58, 57, 56); border-radius:8px; border:solid; border-color: white" class="container login-form">
	<h2 style="text-align: center; color:white" class="login-title">- Please Login -</h2>
	<div class="panel panel-default">
		<div class="panel-body">
			<form action="services/Login.php" method="post" name="login">
				<div class="input-group login-userinput">
					<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
					<input id="txtUser" type="text" class="form-control" name="username"  placeholder="Username" required>
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
					<input  id="txtPassword" type="password" class="form-control" name="password" placeholder="Password" required>
          </span>  
				</div>
				<br>
				<button class="btn btn-success btn-block login-button" name="submit" type="submit" value="Login" onClick="showAlert()"><i style="margin-right: 10px;" class="fa fa-sign-in-alt"></i> <span style="font-weight: bold;"> Login</span></button>
				<div class="checkbox login-options">
					<label><input type="checkbox"/> Remember Me</label>
				</div>		
			</form>
			<br>
			<img class="img img-responsive" src="img/logo.png">			
		</div>
	</div>
</div>
</div>
<script src="scripts/sweetAlert.js"></script>
<script>
	message();
	function message() {
	   var content = document.createElement('div');
       var error = '<?php echo $session_value; ?>';
       if(error == "success"){
        content.innerHTML = 'Login Succesfull, <strong> WelCome! </strong>';
        swal({
            title: "Success",
            content: content,
            icon: "success",
            button: false
        });
       	setTimeout(function(){
          var unsetSession = '<?php unset($_SESSION['error']);?>';
          window.location.href = 'index.php';
         }, 1500);
        }
       else if(error == "failed"){
         content.innerHTML = 'Invalid <strong> Username </strong> or <strong> Password </strong> Used!';
       	 swal({
          title: "Invalid Login!",
          content: content,
          text: "",
          icon: "warning",
          button:false
          });
       setTimeout(function(){
       var unsetSession = '<?php unset($_SESSION['error']);?>';
       window.location.href = 'login.php';
       }, 1500);
       }
	};
</script>
<script src="vendor_/bootstrap/js/bootstrap.min.js"></script>
<script src="scripts/fontawesome.js"></script>
<script src="scripts/fontawesome-all.js"></script>